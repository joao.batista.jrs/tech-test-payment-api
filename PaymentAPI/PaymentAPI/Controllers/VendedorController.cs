﻿using Microsoft.AspNetCore.Mvc;
using Payment.Application.DTOs;
using Payment.Application.Services.Interface;

namespace Payment.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendedorController : ControllerBase
    {
        private readonly IVendedorService _vendedorService;

        public VendedorController(IVendedorService service)
        {
            _vendedorService = service;
        }

        [HttpPost]
        public IActionResult AddVendedorAsync([FromBody] VendedorDto vendedorDto)
        {
            if (vendedorDto == null)
                return NotFound();

            _vendedorService.Create(vendedorDto);

            return Ok("Vendedor Cadastrador Com Sucesso!");
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetByIdAsync(int id)
        {
            var result =  _vendedorService.GetById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }
    }
}

