﻿using Microsoft.AspNetCore.Mvc;
using Payment.Application.DTOs;
using Payment.Application.Services.Interface;
using Payment.Domain.Enums;

namespace Payment.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendaController : ControllerBase
    {
        private readonly IVendaService _vendaService;

        public VendaController(IVendaService vendaService)
        {
            _vendaService = vendaService;
        }

        [HttpPost]
        public IActionResult AddVenda([FromBody] VendaDto vendaDto)
        {
            if (vendaDto == null)
                return BadRequest();
            
            _vendaService.FormataVenda(vendaDto);

            return Ok("Venda Cadastrado com Sucesso!");
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(int id)
        {
            var result = _vendaService.GetById(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateById(int id, StatusVendaEnum status)
        {
            var result = _vendaService.Update(id, status);

            if (result == null)
                return NotFound();

            return Ok(result);
        }
    }
}
