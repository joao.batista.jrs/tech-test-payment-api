﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Models;

namespace Payment.InfraStructure.Context
{
    public class PaymentDbContext : DbContext
    {
        public PaymentDbContext(DbContextOptions<PaymentDbContext> options) : base(options)
        {
        }

        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<ProdutosVenda> ProdutosVenda { get; set; }
    }
}
