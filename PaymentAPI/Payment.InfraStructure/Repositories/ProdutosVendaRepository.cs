﻿using Payment.Domain.Models;
using Payment.Domain.RepositoriesInterface;
using Payment.InfraStructure.Context;

namespace Payment.InfraStructure.Repositories
{
    public class ProdutosVendaRepository : IProdutosVendaRepository
    {
        private readonly PaymentDbContext _paymentDbContext;

        public ProdutosVendaRepository(PaymentDbContext context)
        {
            _paymentDbContext = context;
        }
        public void Create(ProdutosVenda produtosVenda)
        {
            _paymentDbContext.Add(produtosVenda);
            _paymentDbContext.SaveChanges();
        }

        public ProdutosVenda GetById(int id)
        {
            var produtosVendaDb = _paymentDbContext.ProdutosVenda.Find(id);

            if (produtosVendaDb == null)
                return null;

            return produtosVendaDb;
        }
    }
}
