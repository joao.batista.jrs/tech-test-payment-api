﻿using Payment.Domain.Interfaces;
using Payment.Domain.Models;
using Payment.Domain.RepositoriesInterface;
using Payment.InfraStructure.Context;

namespace Payment.InfraStructure.Repositories
{
    public class VendedorRepository : IVendedorRepository
    {
        private readonly PaymentDbContext _paymentDbContext;
        public VendedorRepository(PaymentDbContext context)
        {
            _paymentDbContext = context;
        }

        public int Create(Vendedor vendedor)
        {
            _paymentDbContext.Add(vendedor);
            _paymentDbContext.SaveChanges();

            return vendedor.Id;
        }

        public Vendedor GetById(int id)
        {
            var vendedorDb =  _paymentDbContext.Vendedores.Find(id);

            return vendedorDb;
        }
    }
}
