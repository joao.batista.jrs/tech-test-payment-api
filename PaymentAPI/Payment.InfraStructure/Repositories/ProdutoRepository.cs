﻿using Payment.Domain.Models;
using Payment.Domain.RepositoriesInterface;
using Payment.InfraStructure.Context;

namespace Payment.InfraStructure.Repositories
{
    public class ProdutoRepository : IProdutoRepository
    {
        private readonly PaymentDbContext _paymentDbContext;

        public  ProdutoRepository(PaymentDbContext context)
        {
            _paymentDbContext = context;
        }
            
        public int Create(Produto produto)
        {
           _paymentDbContext.Add(produto);
           _paymentDbContext.SaveChanges();

            return produto.Id;
        }

        public Produto GetById(int id)
        {
            var produtoDb = _paymentDbContext.Produtos.Find(id);

            if (produtoDb == null)
                return null;

            return produtoDb;
        }
    }
}
