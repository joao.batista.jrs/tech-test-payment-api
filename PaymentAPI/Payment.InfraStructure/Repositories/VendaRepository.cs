﻿using Payment.Domain.Enums;
using Payment.Domain.Models;
using Payment.Domain.RepositoriesInterface;
using Payment.InfraStructure.Context;

namespace Payment.InfraStructure.Repositories
{
    public class VendaRepository : IVendaRepository
    {
        private readonly PaymentDbContext _paymentDbContext;
        public VendaRepository(PaymentDbContext context)
        {
            _paymentDbContext = context;
        }
        public int Create(Venda venda)
        {
            var vendaResult = new Venda(venda.VendedorId);

            _paymentDbContext.Add(vendaResult);

            _paymentDbContext.SaveChanges();

            return vendaResult.Id;
        }

        public Venda GetById(int id)
        {
            var vendaDb = _paymentDbContext.Vendas.Find(id);

            if (vendaDb == null)
                return null;

            return vendaDb;
        }

        public Venda Update(int id, StatusVendaEnum status)
        {
            var vendaDb = _paymentDbContext.Vendas.Find(id);
            vendaDb.Status = status;

            _paymentDbContext.Update(vendaDb);
            _paymentDbContext.SaveChanges();

            return vendaDb;
        }
    }
}
