﻿using Payment.Application.DTOs;

namespace Payment.Application.Services.Interface
{
    public interface IVendedorService
    {
        int Create(VendedorDto vendedorDto);
        VendedorDto GetById(int id);
    }
}
