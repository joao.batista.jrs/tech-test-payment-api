﻿using Payment.Application.DTOs;
using Payment.Domain.Models;

namespace Payment.Application.Services.Interface
{
    public interface IProdutoService
    {
        int Create(ProdutoDto produtoDto);
        ProdutoDto GetById(int id);
        List<int> ValidaListaDeProdutos(ICollection<ProdutoDto> produtos);
    }
}
