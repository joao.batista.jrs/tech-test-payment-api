﻿using Payment.Application.DTOs;
using Payment.Domain.Enums;

namespace Payment.Application.Services.Interface
{
    public interface IVendaService
    {
        int Create(VendaDto vendaDto);
        VendaDto GetById(int id);
        void FormataVenda(VendaDto vendaDto);
        VendaDto Update(int id, StatusVendaEnum status);
    }
}
