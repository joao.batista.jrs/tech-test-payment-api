﻿using AutoMapper;
using Payment.Application.DTOs;
using Payment.Application.Services.Interface;
using Payment.Domain.Models;
using Payment.Domain.RepositoriesInterface;

namespace Payment.Application.Services.Service
{
    public class ProdutoService : IProdutoService
    {
        private readonly IProdutoRepository _produtoRepository;
        private readonly IMapper _mapper;

        public ProdutoService(IProdutoRepository produtoRepository, IMapper mapper)
        {
            _produtoRepository = produtoRepository;
            _mapper = mapper;
        }

        public int Create(ProdutoDto produtoDto)
        {
            var produto = _mapper.Map<Produto>(produtoDto);

            var result = _produtoRepository.Create(produto);

            return result;
        }

        public ProdutoDto GetById(int id)
        {
            var produto = _produtoRepository.GetById(id);
            var produtoDto = _mapper.Map<ProdutoDto>(produto);
            return produtoDto;    
        }

        public List<int> ValidaListaDeProdutos(ICollection<ProdutoDto> produtos)
        {
            var listaIds = new List<int>();
            foreach (var produto in produtos)
            {
                if (produto.Id == 0)
                {
                    var produtoNovo = _mapper.Map<Produto>(produto);
                    var result = _produtoRepository.Create(produtoNovo);
                    listaIds.Add(result);
                }
                else
                {
                    listaIds.Add(produto.Id);
                }
            }

            return listaIds;
        }
    }
}
