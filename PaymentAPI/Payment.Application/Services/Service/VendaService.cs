﻿using AutoMapper;
using Payment.Application.DTOs;
using Payment.Application.Services.Interface;
using Payment.Domain.Enums;
using Payment.Domain.Interfaces;
using Payment.Domain.Models;
using Payment.Domain.RepositoriesInterface;

namespace Payment.Application.Services.Service
{
    public class VendaService : IVendaService
    {
        private readonly IVendaRepository _vendaRepository;
        private readonly IProdutosVendaRepository _produtosVendaRepository;
        private readonly IProdutoService _produtoService;
        private readonly IVendedorService _vendedorService;
        private readonly IMapper _mapper;        

        public VendaService(IVendaRepository vendaRepository, IProdutosVendaRepository produtosVendaRepository, 
            IProdutoService produtoService, IVendedorService vendedorService, IMapper mapper)
        {
            _vendaRepository = vendaRepository;
            _produtoService = produtoService;
            _vendedorService = vendedorService;
            _produtosVendaRepository = produtosVendaRepository;
            _mapper = mapper;
        }

        public int Create(VendaDto vendaDto)
        {
            var venda = _mapper.Map<Venda>(vendaDto);

            var vendaSalva =  _vendaRepository.Create(venda);

            return vendaSalva;
        }

        public VendaDto GetById(int id)
        {
            var venda = _vendaRepository.GetById(id);

            return _mapper.Map<VendaDto>(venda);
        }

        public void FormataVenda(VendaDto vendaDto)
        {
            var idsProdutos = _produtoService.ValidaListaDeProdutos(vendaDto.Produtos);

            vendaDto.VendedorId = ValidaVendedor(vendaDto);

            var idVenda = Create(vendaDto);

            AdicionaProdutosVendas(idVenda, idsProdutos);
        }

        public VendaDto Update(int id, StatusVendaEnum status)
        {
            var venda = _vendaRepository.GetById(id);
            
            if(ValidaStatus(venda, status))
                _vendaRepository.Update(id, status);            

            return _mapper.Map<VendaDto>(venda);
        }

        private void AdicionaProdutosVendas(int vendaId, List<int> produtosId)
        {
            foreach (var produto in produtosId)
            {
                var ProdutosVenda = new ProdutosVenda();
                ProdutosVenda.ProdutoId = produto;
                ProdutosVenda.VendaId = vendaId;
                _produtosVendaRepository.Create(ProdutosVenda);                
            }
        }

        private int ValidaVendedor(VendaDto vendaDto)
        {
            var idVendedor = vendaDto.VendedorId;

            if (idVendedor == 0)
                idVendedor = _vendedorService.Create(vendaDto.Vendedor);
            else
                return idVendedor;
            
            return idVendedor;
        }

        private bool ValidaStatus(Venda venda, StatusVendaEnum status)
        {
            var vendaDb = venda;

            if (venda.Status == StatusVendaEnum.AguardandoPagamento &&
                status == StatusVendaEnum.PagamentoAprovado ||
                status == StatusVendaEnum.Cancelada)
            {
                return true;
            }
            else if (venda.Status == StatusVendaEnum.PagamentoAprovado &&
                status == StatusVendaEnum.EnviadoTransportadora ||
                status == StatusVendaEnum.Cancelada)
            {
                return true;
            }
            else if (venda.Status == StatusVendaEnum.EnviadoTransportadora &&
                status == StatusVendaEnum.Entregue)
            {
                return true;
            }
            else
                throw new Exception("Transição de status inválida");
        }
    }
}
