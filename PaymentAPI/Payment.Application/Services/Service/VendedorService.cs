﻿using AutoMapper;
using Payment.Application.DTOs;
using Payment.Application.Services.Interface;
using Payment.Domain.Models;
using Payment.Domain.RepositoriesInterface;

namespace Payment.Application.Services.Service
{
    public class VendedorService : IVendedorService
    {
        private readonly IVendedorRepository _vendedorRepository;
        private readonly IMapper _mapper;

        public VendedorService(IVendedorRepository vendedorRepository, IMapper mapper)
        {
            _vendedorRepository = vendedorRepository;
            _mapper = mapper;
        }

        public int Create(VendedorDto vendedorDto)
        {
            var vendedor = _mapper.Map<Vendedor>(vendedorDto);
            var vendedorId = _vendedorRepository.Create(vendedor);

            return vendedorId;
        }

        public VendedorDto GetById(int id)
        {
            var vendedor = _vendedorRepository.GetById(id);

            return _mapper.Map<VendedorDto>(vendedor);
        }

        public void ValidaVendedor(VendedorDto vendedorDto)
        {
            if(vendedorDto.Id == 0)
                Create(vendedorDto);
        }
    }
}
