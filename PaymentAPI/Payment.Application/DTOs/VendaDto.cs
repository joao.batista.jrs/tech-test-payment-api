﻿using Payment.Domain.Enums;
using Payment.Domain.Models;

namespace Payment.Application.DTOs
{
    public class VendaDto
    {
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }
        public StatusVendaEnum Status { get; set; }
        public int VendedorId { get; set; }
        public VendedorDto Vendedor { get; set; }
        public ICollection<ProdutoDto> Produtos { get; set; }
    }
}
