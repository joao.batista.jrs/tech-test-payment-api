﻿namespace Payment.Application.DTOs
{
    public class ProdutosVendaDto
    {
        public int VendaId { get; set; }
        public int ProdutoId { get; set; }
    }
}
