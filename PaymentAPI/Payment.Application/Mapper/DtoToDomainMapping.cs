﻿using AutoMapper;
using Payment.Application.DTOs;
using Payment.Domain.Models;

namespace Payment.Application.Mapper
{
    public class DtoToDomainMapping : Profile
    {
        public DtoToDomainMapping()
        {
            CreateMap<ProdutoDto, Produto>();
            CreateMap<VendaDto, Venda>();
            CreateMap<VendedorDto, Vendedor>();
        }
    }
}
