﻿using AutoMapper;
using Payment.Application.DTOs;
using Payment.Domain.Models;

namespace Payment.Application.Mapper
{
    public class DomainToDtoMapping : Profile
    {
        public DomainToDtoMapping()
        {
            CreateMap<Produto, ProdutoDto>();
            CreateMap<Vendedor, VendedorDto>();
            CreateMap<Venda, VendaDto>();
        }
    }
}
