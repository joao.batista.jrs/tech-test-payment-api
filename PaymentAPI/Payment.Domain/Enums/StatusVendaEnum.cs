﻿namespace Payment.Domain.Enums
{
    public enum StatusVendaEnum
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoTransportadora,
        Entregue,
        Cancelada
    }
}
