﻿using Payment.Domain.Models;

namespace Payment.Domain.RepositoriesInterface
{
    public interface IProdutoRepository
    {
        Produto GetById(int id);
        int Create(Produto produto);
    }
}