﻿using Payment.Domain.Models;

namespace Payment.Domain.RepositoriesInterface
{
    public interface IProdutosVendaRepository
    {
        ProdutosVenda GetById(int id);
        void Create(ProdutosVenda produtosVenda);
    }
}
