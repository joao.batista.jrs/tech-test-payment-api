﻿using Payment.Domain.Enums;
using Payment.Domain.Models;

namespace Payment.Domain.RepositoriesInterface
{
    public interface IVendaRepository
    { 
        Venda GetById(int id);
        int Create(Venda venda);
        Venda Update(int id, StatusVendaEnum status);
    }
}
