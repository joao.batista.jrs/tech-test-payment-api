﻿using Payment.Domain.Models;

namespace Payment.Domain.RepositoriesInterface
{
    public interface IVendedorRepository
    {
        Vendedor GetById(int id);
        int Create(Vendedor vendedor);
    }
}
