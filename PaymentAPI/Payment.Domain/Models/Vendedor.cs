﻿using Payment.Domain.Interfaces;

namespace Payment.Domain.Models
{
    public class Vendedor : IEntityBase, IVendedor
    {
        public Vendedor(string nome, string cPF, string email, string telefone)
        {
            Nome = nome;
            CPF = cPF;
            Email = email;
            Telefone = telefone;
        }

        public Vendedor()
        { }
        public int Id { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
