﻿using Payment.Domain.Enums;
using Payment.Domain.Interfaces;

namespace Payment.Domain.Models
{
    public class Venda : IEntityBase, IVenda
    {
        public Venda(int vendedorId)
        {
            DataVenda = DateTime.UtcNow;
            Status = StatusVendaEnum.AguardandoPagamento;
            VendedorId = vendedorId;
        }

        public int Id { get; set; }
        public DateTime DataVenda { get; set; }
        public StatusVendaEnum Status { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public ICollection<Produto> Produtos { get; set; }
    }
}
