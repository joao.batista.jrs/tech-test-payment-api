﻿using Payment.Domain.Interfaces;

namespace Payment.Domain.Models
{
    public class Produto : IEntityBase, IProduto
    {
        public int Id { get; set; }
        public string Descricao { get; set ; }
        public decimal Preco { get; set; }
    }
}
