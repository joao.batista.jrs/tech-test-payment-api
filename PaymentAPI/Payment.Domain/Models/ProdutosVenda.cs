﻿using Payment.Domain.Interfaces;

namespace Payment.Domain.Models
{

    public class ProdutosVenda : IEntityBase, IProdutosVenda
    {
        public int Id { get; set; }
        public int VendaId { get; set; }
        public Venda Venda { get; set; }    
        public int ProdutoId { get; set; }
        public Produto Produto { get; set; }
    }
}
