﻿using Payment.Domain.Enums;
using Payment.Domain.Models;

namespace Payment.Domain.Interfaces
{
    public interface IVenda
    {
        public DateTime DataVenda { get; set; }
        public StatusVendaEnum Status { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public ICollection<Produto> Produtos { get; set; }
    }
}
