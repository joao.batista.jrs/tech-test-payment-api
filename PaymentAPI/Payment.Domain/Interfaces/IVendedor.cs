﻿namespace Payment.Domain.Interfaces
{
    public interface IVendedor
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Email{ get; set; }
        public string Telefone { get; set; }
    }
}
