﻿namespace Payment.Domain.Interfaces
{
    public interface IProduto
    {
        public string Descricao { get; }
        public decimal Preco { get; }
    }
}
