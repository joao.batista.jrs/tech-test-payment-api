﻿namespace Payment.Domain.Interfaces
{
    public interface IEntityBase
    {
        public int Id { get; }
    }
}