﻿using Payment.Domain.Models;

namespace Payment.Domain.Interfaces
{
    public interface IProdutosVenda
    {
        public int VendaId { get; set; }
        public Produto Produto { get; set; }
    }
}
