﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Payment.Application.Mapper;
using Payment.Application.Services.Interface;
using Payment.Application.Services.Service;
using Payment.Domain.RepositoriesInterface;
using Payment.InfraStructure.Context;
using Payment.InfraStructure.Repositories;

namespace Payment.IoC
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfraStrucuture(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<PaymentDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("PaymentApiConnectionString")));
            services.AddScoped<IVendaRepository, VendaRepository>();
            services.AddScoped<IVendedorRepository, VendedorRepository>();
            services.AddScoped<IProdutoRepository, ProdutoRepository>();
            services.AddScoped<IProdutosVendaRepository, ProdutosVendaRepository>();

            return services;
        }
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(typeof(DomainToDtoMapping));
            services.AddScoped<IProdutoService, ProdutoService>();
            services.AddScoped<IVendedorService, VendedorService>();
            services.AddScoped<IVendaService, VendaService>();

            return services;
        }
    }
}